<?php
session_start();
require ('../model/database_class.php');
require ('../model/course_class.php');
require ('../model/grade_class.php');

$db = new Database();
$grade = new Grade($db);
$course = new Course($db);
$courses = $course->getCourses();
$studentGrade = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $studentId = $_POST['student_id'];
    $courseKey = $_POST['course_key'];

    if (empty($studentId) || !is_numeric($studentId)) {
        echo "Student ID cannot be empty or non-numeric.";
    } else {
        $result = $grade->getGrade($studentId, $courseKey);
        if ($result) {
            $studentGrade = $result['grade'];
        } else {
            echo "No grade found for this student ID in this course.";
        }
    }
}


?>

<?php
session_start();
var_dump('controller');
require('./../model/database_class.php');
require('./../model/course_class.php');
// require ('../model/course_class.php');
// require ('../model/database_class.php');
// require ('../model/grade_class.php');
$db = new Database();
$course = new Course($db);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $courseName = $_POST['course_name'];
    if (empty($courseName) || is_numeric($courseName)) {
        echo "Course name cannot be empty or numeric.";
    } else {
        echo $course->addCourse($courseName);
    }
}
?>

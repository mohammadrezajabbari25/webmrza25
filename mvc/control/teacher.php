<?php
// var_dump('gggg');
session_start();
require ('../model/database_class.php');
require ('../model/grade_class.php');
// var_dump('slsls');
require ('../model/course_class.php');
$db = new Database();
$grade = new Grade($db);
$course= new Course($db);
// var_dump('gggg');
$courses = $course->getCourses();
// var_dump('sdas');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $studentId = $_POST['student_id'];
    $courseKey = $_POST['course_key'];
    $studentGrade = $_POST['grade'];

    if (empty($studentId) || !is_numeric($studentId)) {
        echo "Student ID cannot be empty or non-numeric.";
    } elseif (empty($studentGrade) || !is_numeric($studentGrade) || $studentGrade < 0 || $studentGrade > 100) {
        echo "Grade must be a number between 0 and 100.";
    } else {
        echo $grade->saveGrade($studentId, $courseKey, $studentGrade);
    }
}
// var_dump('sdasssss');


?>

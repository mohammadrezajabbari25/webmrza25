<?php
// db_creation.php

$servername = "localhost:3307";
$username = "root";
$password = "root";
$dbname = "school_db";

try {
    // Create connection
    $conn = new PDO("mysql:host=$servername", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->exec("CREATE DATABASE IF NOT EXISTS $dbname");
    $conn->exec("USE $dbname");

    $sql = "CREATE TABLE IF NOT EXISTS courses (
        course_key INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        course_name VARCHAR(50) NOT NULL
    )";
    $conn->exec($sql);

    $sql = "CREATE TABLE IF NOT EXISTS grades (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        student_id INT(6) NOT NULL,
        course_key INT(6) UNSIGNED NOT NULL,
        grade DECIMAL(4, 2) NOT NULL,
        FOREIGN KEY (course_key) REFERENCES courses(course_key)
    )";
    $conn->exec($sql);
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
    exit(); 
}


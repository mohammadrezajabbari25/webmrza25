<?php
// require ('./database_class.php');

class Course {
    private $db;

    public function __construct(Database $db) {
        $this->db = $db->conn;
    }

    public function addCourse($courseName) {
        $stmt = $this->db->prepare("SELECT course_name FROM courses WHERE course_name = :course_name");
        $stmt->bindParam(':course_name', $courseName);
        $stmt->execute();

        if ($stmt->fetchColumn()) {
            return "The course name already exists. Please choose a different name.";
        } else {
            $stmt = $this->db->prepare("INSERT INTO courses (course_name) VALUES (:course_name)");
            $stmt->bindParam(':course_name', $courseName);
            $stmt->execute();
            return "Course added successfully!";
        }
    }

    public function getCourses(): array {
        $stmt = $this->db->query("SELECT course_key, course_name FROM courses");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
?>

<?php
class Database {
    private $servername = "localhost:3307";
    private $username = "root";
    private $password = "root";
    private $dbname = "school_db";
    public $conn;

    public function __construct() {
        try {
            $this->conn = new PDO("mysql:host={$this->servername}", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->createDatabase();
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            exit();
        }
    }

    private function createDatabase() {
        $this->conn->exec("CREATE DATABASE IF NOT EXISTS {$this->dbname}");
        $this->conn->exec("USE {$this->dbname}");
    }

    public function createTables() {
        $courseTable = "
            CREATE TABLE IF NOT EXISTS courses (
                course_key INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                course_name VARCHAR(50) NOT NULL
            )";
        $this->conn->exec($courseTable);

        $gradesTable = "
            CREATE TABLE IF NOT EXISTS grades (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                student_id INT(6) NOT NULL,
                course_key INT(6) UNSIGNED NOT NULL,
                grade DECIMAL(4, 2) NOT NULL,
                FOREIGN KEY (course_key) REFERENCES courses(course_key)
            )";
        $this->conn->exec($gradesTable);
    }
}
?>

<?php
class Grade {
    private $db;

    public function __construct(Database $db) {
        $this->db = $db->conn;
    }

    public function saveGrade($studentId, $courseKey, $grade) {
        $stmt = $this->db->prepare("SELECT grade FROM grades WHERE student_id = :student_id AND course_key = :course_key");
        $stmt->bindParam(':student_id', $studentId);
        $stmt->bindParam(':course_key', $courseKey);
        $stmt->execute();

        if ($stmt->fetchColumn()) {
            $stmt = $this->db->prepare("UPDATE grades SET grade = :grade WHERE student_id = :student_id AND course_key = :course_key");
            $stmt->bindParam(':grade', $grade);
            $stmt->bindParam(':student_id', $studentId);
            $stmt->bindParam(':course_key', $courseKey);
            $stmt->execute();
            return "Grade updated successfully.";
        } else {
            $stmt = $this->db->prepare("INSERT INTO grades (student_id, course_key, grade) VALUES (:student_id, :course_key, :grade)");
            $stmt->bindParam(':student_id', $studentId);
            $stmt->bindParam(':course_key', $courseKey);
            $stmt->bindParam(':grade', $grade);
            $stmt->execute();
            return "Grade saved successfully.";
        }
    }

    public function getGrade($studentId, $courseKey) {
        $stmt = $this->db->prepare("SELECT grade FROM grades WHERE student_id = :student_id AND course_key = :course_key");
        $stmt->bindParam(':student_id', $studentId);
        $stmt->bindParam(':course_key', $courseKey);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
?>

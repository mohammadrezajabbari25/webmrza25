<?php
require('../control/student.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Student Grade</title>
</head>
<body>
    <h2>View Student Grade</h2>
    <form method="post" action="">
        <label for="student_id">Student ID:</label>
        <input type="text" id="student_id" name="student_id" required>
        <label for="course_key">Select Course:</label>
        <select id="course_key" name="course_key" required>
            <option value="">Select a course</option>
            <?php foreach ($courses as $course): ?>
                <option value="<?= $course['course_key']; ?>"><?= htmlspecialchars($course['course_name']); ?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" value="submit">
    </form>
    <?php if (!empty($studentGrade)): ?>
        <p class="result">The grade for this student is: <?= htmlspecialchars($studentGrade); ?></p>
    <?php elseif ($_SERVER["REQUEST_METHOD"] == "POST" && empty($studentGrade)): ?>
        <p class="result">No grade found for this student in the selected course.</p>
    <?php endif; ?>
</body>
</html>

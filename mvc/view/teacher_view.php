<?php
    require('./../control/teacher.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Grade Submission</title>
</head>
<body>
    <h2>Submit Grade</h2>
    <form method="post" action="./teacher_view.php">
        Student ID: <input type="text" name="student_id" required><br>
        Course: 
        <select name="course_key" required>
            <?php
            foreach ($courses as $course) {
                echo "<option value='{$course['course_key']}'>{$course['course_name']}</option>";
            }
            ?>
        </select><br>
        Grade: <input type="text" name="grade" required><br>
        <input type="submit" value="Submit Grade">
    </form>
</body>
</html>
